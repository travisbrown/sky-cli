# Sky CLI

A command-line application for the BlueSky API

## Current functions

Currently, this application queries the Bluesky API for a specific user's posts and writes them to a SQLite database at `./sky.db`. It will probably do something fancier in the future.

## Setup

### Pre-requisites

This is a Rust application. you will need to [install Rust](https://www.rust-lang.org/tools/install) in order to compile and run it.

### Environment Variables

You will need to define the following environment variables

| Name          | Description                                                                                  | Example             |
| ------------- | -------------------------------------------------------------------------------------------- | ------------------- |
| BSKY_SERVICE  | The URL of the Bluesky service you're connecting to                                          | https://bsky.social |
| BSKY_USERNAME | The username used to log in to Bluesky (usually your username)                               | breen.tech          |
| BSKY_PASSWORD | An [app password](https://staging.bsky.app/settings/app-passwords) used to log in to Bluesky | not-a-real-password |

## Ok, how do I run this thing?

### Commands

#### Archive

Run the application with the `archive` command and pass in the username whose posts you want to archive:

```sh
# Let's look for posts from the breen.tech user (usernames are domain names)
cargo run -- archive breen.tech
```
When the application is finished, you should have a SQLite Database at `./sky.db`. It currently has a `post` table in it which is where the posts get written. It also has a `profile` table with user profile info in it.

You can browse this database with a tool like [sqlitebrowser](https://sqlitebrowser.org/dl/).

If you run this application multiple times for different users, it will store the user's profile information in the `profile` table and store all of those users' posts in the `post` table.

#### List Handles

Run the application with the `list-handles` command and pass in a username/handle:

```sh
# Let's list handles for the breen.tech user
cargo run -- list-handles breen.tech

Fetching user profile: breen.tech
User's DiD is did:plc:l2ktomvijz42t6hfqaxl7gq6
[
  {
    "did": "did:plc:l2ktomvijz42t6hfqaxl7gq6",
    "handle": "breen.tech",
    "display_name": "Breen",
    "archived_at": "2023-05-13T05:43:31Z"
  }
]
```
This command will find the DiD of the user with this handle and return all of the `Profile` objects in the application database with that DiD.

This is useful if you have been archiving a particular user over time and you want to see if they have used any previous handles/usernames

**For now, this command will only return data if you've previously archived the DiD associated with the user you search. If you want to track a user's usernames over time, you need to archive their posts regularly**

#### List Posts

Run the application with the `list-posts` command and pass in a username/handle:

```sh
# Let's list posts for the breen.tech user
cargo run -- list-posts breen.tech

Fetching user profile: breen.tech
User's DiD is did:plc:l2ktomvijz42t6hfqaxl7gq6
[
  ...posts are here in JSON format
]
```
This command will find the DiD of the user with this handle and return all of the `Post` objects in the application database with that DiD.

**For now, this command will only return data if you've previously archived the DiD associated with the user you search.**

## Acknowledgements

I got started on this with help from [this video by Chris Biscardi](https://www.youtube.com/watch?v=_EUNtMHcxP4)

I borrowed heavily from the examples in the [Bisky API Library source code](https://github.com/jesopo/bisky)
