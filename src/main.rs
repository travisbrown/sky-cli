use bisky::atproto::{Client, Session};
use bisky::bluesky::Bluesky;
use bisky::storage::{File, Storage as _};
use clap::Parser;

use std::path::PathBuf;
use std::str::FromStr;

mod db;
mod config;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct ArchiveArgs {
    /// Command - unused but has to be here
    #[clap(index = 1)]
    command: String,

    /// Username to get posts for
    #[clap(index = 2)]
    query: String,
}

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct ListHandlesArgs {
    /// Command - unused but has to be here
    #[clap(index = 1)]
    command: String,

    /// Username to print handles for
    #[clap(index = 2)]
    user: String,
}

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct ListPostsArgs {
    /// Command - unused but has to be here
    #[clap(index = 1)]
    command: String,

    /// Username to print handles for
    #[clap(index = 2)]
    user: String,
}

async fn archive_user(config: config::Config) {
    let args = ArchiveArgs::parse();
    
    println!("Creating database");
    db::create_tables().unwrap();

    println!("Logging in to bluesky");
    let mut storage = File::<Session>::new(PathBuf::from_str("/tmp/jwt.json").unwrap());
    if storage.get().await.is_err() {
        Client::login(&config.service_url, &config.username, &config.password, &mut storage)
            .await
            .unwrap();
    }
    let mut client = Bluesky::new(Client::new(config.service_url, storage).await.unwrap());
    
    println!("Fetching user profile: {}", args.query);
    let mut user = client.user(args.query);
    let profile = user.get_profile().await.unwrap();
    db::insert_profile(&profile).unwrap();

    println!("Fetching posts");
    let posts = user.list_posts().await.unwrap();
    println!("Found {} posts", posts.len());

    println!("Storing posts to db");
    db::insert_posts(&posts, &profile).unwrap();
}

async fn list_handles(config: config::Config) {
    let args =ListHandlesArgs::parse();

    println!("Listing handles for {}", args.user);
    
    // Fetch DiD for the handle
    println!("Logging in to bluesky");
    let mut storage = File::<Session>::new(PathBuf::from_str("/tmp/jwt.json").unwrap());
    if storage.get().await.is_err() {
        Client::login(&config.service_url, &config.username, &config.password, &mut storage)
            .await
            .unwrap();
    }
    let mut client = Bluesky::new(Client::new(config.service_url, storage).await.unwrap());
    
    println!("Fetching user profile: {}", args.user);
    let mut user = client.user(args.user);
    let profile = user.get_profile().await.unwrap();
    println!("User's DiD is {}", profile.did);
    let user_handles = db::get_profiles(profile.did).unwrap();
    println!("{}", serde_json::to_string_pretty(&user_handles).unwrap());
    // Ok(())
}

async fn list_posts(config: config::Config) {
    let args = ListPostsArgs::parse();

    println!("Listing posts for {}", args.user);
    
    // Fetch DiD for the handle
    println!("Logging in to bluesky");
    let mut storage = File::<Session>::new(PathBuf::from_str("/tmp/jwt.json").unwrap());
    if storage.get().await.is_err() {
        Client::login(&config.service_url, &config.username, &config.password, &mut storage)
            .await
            .unwrap();
    }
    let mut client = Bluesky::new(Client::new(config.service_url, storage).await.unwrap());
    
    println!("Fetching user profile: {}", args.user);
    let mut user = client.user(args.user);
    let profile = user.get_profile().await.unwrap();
    println!("User's DiD is {}", profile.did);
    let user_posts = db::get_posts(profile.did).unwrap();
    println!("{}", serde_json::to_string_pretty(&user_posts).unwrap());
    // Ok(())
}

#[tokio::main]
async fn main() {
    let config = config::parse_config().unwrap();

    let command = std::env::args().nth(1).expect("no command given");
    match command.as_str() {
        "archive" => archive_user(config).await,
        "list-handles" => list_handles(config).await,
        "list-posts" => list_posts(config).await,
        _ => println!("Unrecognized command: {}", command)
    }
}
