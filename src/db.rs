use bisky::lexicon::app::bsky::actor::ProfileViewDetailed;
use bisky::lexicon::app::bsky::feed::Post;
use bisky::lexicon::com::atproto::repo::Record;
use chrono::{DateTime, Utc};
use rusqlite::{Connection, Result};
use serde::{Deserialize, Serialize};

pub fn create_tables() -> Result<()> {
    let connection: Connection = Connection::open("./sky.db").unwrap();
    connection.execute(
        "CREATE TABLE IF NOT EXISTS post (
            cid    TEXT PRIMARY KEY,
            uri    TEXT NOT NULL,
            created_at TIMESTAMP NOT NULL,
            text TEXT NOT NULL,
            author_did TEXT NOT NULL,
            archived_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
        )",

        (),
    )?;

    connection.execute(
        "CREATE TABLE IF NOT EXISTS profile (
            did    TEXT NOT NULL,
            handle    TEXT NOT NULL,
            display_name TEXT,
            archived_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (did, handle)
        )",

        (),
    )?;

    connection.close().unwrap();

    Ok(())
}

pub fn insert_posts(posts: &Vec<Record<Post>>, profile: &ProfileViewDetailed) -> Result<()> {
    let connection: Connection = Connection::open("./sky.db").unwrap();

    for post in posts.iter() {
        connection.execute(
            "INSERT OR IGNORE INTO post (cid, uri, created_at, text, author_did) VALUES (?1, ?2, ?3, ?4, ?5)",
            (&post.cid, &post.uri, &post.value.created_at, &post.value.text, &profile.did),
        )?;
    }

    connection.close().unwrap();

    Ok(())
}

#[derive(Serialize, Deserialize)]
pub struct ArchivedPost {
    cid: String,
    uri: String,
    created_at: DateTime<Utc>,
    text: String,
    author_did: String,
    archived_at: DateTime<Utc>
}

pub fn get_posts(did: String) -> Result<Vec<ArchivedPost>>{
    let connection: Connection = Connection::open("./sky.db").unwrap();

    let mut stmt = connection.prepare("SELECT cid, uri, created_at, text, author_did, archived_at FROM post where author_did=? order by created_at asc")?;
    let post_iter = stmt.query_map([did], |row| {
        Ok(ArchivedPost {
            cid: row.get(0)?,
            uri: row.get(1)?,
            created_at: row.get(2)?,
            text: row.get(3)?,
            author_did: row.get(4)?,
            archived_at: row.get(5)?,
        })
    })?;

    post_iter.collect()
}

pub fn insert_profile(profile: &ProfileViewDetailed) -> Result<()> {
    let connection: Connection = Connection::open("./sky.db").unwrap();

    connection.execute(
        "INSERT OR IGNORE INTO profile (did, handle, display_name) VALUES (?1, ?2, ?3)",
        (&profile.did, &profile.handle, &profile.display_name),
    )?;

    connection.close().unwrap();

    Ok(())
}

#[derive(Serialize, Deserialize)]
pub struct Profile {
    did: String,
    handle: String,
    display_name: String,
    archived_at: DateTime<Utc>
}

pub fn get_profiles(did: String) -> Result<Vec<Profile>>{
    let connection: Connection = Connection::open("./sky.db").unwrap();

    let mut stmt = connection.prepare("SELECT did, handle, display_name, archived_at FROM profile where did=?")?;
    let profile_iter = stmt.query_map([did], |row| {
        Ok(Profile {
            did: row.get(0)?,
            handle: row.get(1)?,
            display_name: row.get(2)?,
            archived_at: row.get(3)?,
        })
    })?;

    profile_iter.collect()
}
